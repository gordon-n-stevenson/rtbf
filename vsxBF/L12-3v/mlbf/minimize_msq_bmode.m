function [img_opt, w_opt] = minimize_msq_bmode(img, ref)

img_opt = 0*img;
[~,~,ni] = size(img);
w_opt = zeros(ni, 1, 'single');

for i = 1:ni
	I = reshape(img(:,:,i),[],1);
	R = reshape(ref(:,:,i),[],1);
	w_opt(i) = abs(sum(I.*R) / sum(I.*I));
	img_opt(:,:,i) = img(:,:,i) * w_opt(i);
end

