/**
 @file vsxBF/utils/VSXDataFormatter.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-27

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef VSXDATAFORMATTER_CUH_
#define VSXDATAFORMATTER_CUH_

#include "DataProcessor.cuh"
#include "HilbertTransform.cuh"
#include "gpuBF.cuh"

namespace rtbf {

/// @brief Enum class to describe VSX sampling mode
enum class VSXSampleMode { BS50BW = 1, BS100BW = 2, NS200BW = 4, HILBERT = 0 };

/** @brief Enum class to describe data ordering

When performing Doppler acquisitions, five dimensions of data are acquired:
- Samples (i.e., fast time)
- Pulses (e.g., pulse-inversion harmonic imaging, averaging to improve SNR)
- Transmits (e.g., focused transmits or plane waves)
- Doppler Frames (i.e., the ensemble dimension)
- Channels

The sample dimension is always the fastest changing and the channel dimension is
always the slowest changing. Pulses, transmits, and Doppler frames can be
permuted arbitrarily depending on the application. DopplerFormat enumerates the
various permutations.

The following formats are supported:
  1. PXF = [Pulses, Transmits, Frames]  e.g., "ultrafast" Doppler
  2. PFX = [Pulses, Frames, Transmits]  e.g., traditional Doppler
  3. XPF = [Transmits, Pulses, Frames]
  4. FPX = [Frames, Pulses, Transmits]
  5. XFP = [Transmits, Frames, Pulses]
  6. FXP = [Frames, Transmits, Pulses]
*/
enum class VSXDataOrder { PXF, PFX, XPF, FPX, XFP, FXP };

/** @brief Class to format Verasonics channel data

This class accepts a host pointer to the raw receive channel buffer, copies it
over to a device pointer, and applies preprocessing.

There are three types of preprocessing that can be applied:
  1. Direct baseband sampling via Verasonics's sampleMode parameter
  2. Coherent summing of pulses (e.g., pulse-inversion harmonic imaging)
  3. Channel mapping unwrapping

The Verasonics scanner provides the option to directly sample the IQ baseband
data, via setting the "Receive.sampleMode" parameter. (See the Verasonics
Sequence Programming Manual for more details.) These modes can be selected by
setting the VSXSampleMode. Supported values are: NS200BW, BS100BW, and BS50BW.
Alternatively, the user can choose to treat the signals as the real component of
the RF signal and obtain the modulated IQ signal via the Hilbert transform using
the value HILBERT.

The class also provides the option to sum (i.e. coherently compound) multiple
input pulses. This is useful for cases like pulse-inversion harmonic imaging,
where the positive and negative phase pulses can be summed immediately to reduce
the data size.

Often, the channel mapping is some permutation of the element positions. When an
optional host pointer to the channel mapping is specified, the channels are
unwrapped automatically. Furthermore, the number of channels per acquisition may
differ from the total number of channels to be stored for processing (e.g.,
walked aperture, multiplexed transducers). In these cases, the user should
provide a mapping of acquired channels to actual channels on a per-transmit
basis via the h_channelMapping argument.

These operations are all fused into a single kernel to minimize the overhead
associated with launching a CUDA kernel.
*/

template <typename T>
class VSXDataFormatter : public DataProcessor<T> {
 protected:
  // VSXDataFormatter members
  int nsamps;   ///< Number of samples per transmit/receive event
  int nxmits;   ///< Number of transmit/receive events to store
  int nchans;   ///< Number of total channels (i.e., number of elements)
  int nacqch;   ///< Number of channels acquired at a time
  int npulses;  ///< Number of pulses to sum (e.g., pulse-inversion imaging)
  int nframes;  ///< Number of frames to process (e.g., Doppler ensemble length)
  VSXSampleMode mode;      ///< Data sample mode (e.g., NS200BW)
  DataArray<short> in;     ///< Input unformatted data (int16)
  DataArray<int> chanmap;  ///< Channel mapping (0-indexed, nacqch-by-nxmits)
  HilbertTransform<T> H;   ///< HilbertTransform object, if mode == HILBERT
  int ppitch;
  int xpitch;
  int fpitch;

  void getPitches(int *pstride, int *xstride, int *fstride, VSXDataOrder order);

 public:
  VSXDataFormatter();
  virtual ~VSXDataFormatter();

  /// @brief Free all dynamically allocated memory.
  void reset();

  /// @brief Initialization function
  void initialize(int numSamples, int numTransmits, int numChannels,
                  int numAcqChannels, int numFrames = 1,
                  VSXSampleMode sampleMode = VSXSampleMode::HILBERT,
                  int numPulsesToSum = 1, int *h_channelMapping = nullptr,
                  int deviceID = 0, cudaStream_t cudaStream = 0,
                  int verbosity = 1);

  /// @brief Function to execute formatting.
  void formatRawVSXData(short *h_raw, int pitchInSamps);
};

namespace VSXDataFormatterKernels {
// CUDA kernels
template <typename T>
__global__ void formatRF(int nsamps, int npulses, int nxmits, int nframes,
                         int nacqch, int nchans, int pstride, int xstride,
                         int fstride, short *idata, int ipitch, T *odata,
                         int opitch, int *cmap);
// NOTE: Templates parameters (like spc) are used instead of function arguments
// because this allows the compiler to optimize away pieces of code that are
// unused at compile-time instead of at run-time. An alternative would be to
// make a new function for each possible value of the parameter, but much of
// this code would be repetitive.
// The template allows us to reuse code that is common to each branch.
template <typename T, int spc>
__global__ void formatIQ(int nsamps, int npulses, int nxmits, int nframes,
                         int nacqch, int nchans, int pstride, int xstride,
                         int fstride, short2 *idata, int ipitch, T *odata,
                         int opitch, int *cmap);

}  // namespace VSXDataFormatterKernels
}  // namespace rtbf

#endif
