# rtbf - Real-time beamforming for ultrasound using GPUs

`rtbf` is a GPU-based implementation of ultrasound beamforming, with the goal of implementing custom algorithms for real-time imaging.

![images/phantom1.gif](images/phantom1.gif) ![images/phantom2.gif](images/phantom2.gif)

Examples of real-time imaging using a custom [speckle-reducing neural network beamformer](https://gitlab.com/dongwoon.hyun/nn_bmode).

## Motivation

Ultrasound scanners produce large amounts of raw data that are then processed to reconstruct images. Previously, application-specific hardware was used to process and downsample the data into a more manageable form (e.g., apply time delays and sum channels). With recent technological advances, especially in graphics processing unit (GPU)-based computing, it is now feasible to obtain ultrasound data from an earlier point in the processing pipeline and perform this beamforming entirely in software at real-time rates. Real-time software beamforming allows the direct implementation and study of experimental algorithms designed to improve ultrasound imaging.

## Code structure

This repository utilizes the CUDA C/C++ API created by NVIDIA and consists of `gpuBF`, `vsxBF`, and `annBF`.

### gpuBF

`gpuBF` is a pure CUDA C/C++ library of beamforming classes. The code is structured around two main classes: `DataArray` and `DataProcessor`.

`DataArray` is a class template that encapsulates a pointer to GPU memory as well as to the descriptors of this memory (e.g., GPU index, pitch). A `DataArray` also provides convenience functions to facilitate data transfer between the host and GPU device (e.g., `copyToGPU`, `copyFromGPUAsync`). `DataArray`s are represented as 3-dimensional arrays with dimensions `(nx, ny, nc)`, where `nx` and `ny` are two spatial dimensions and `nc` is the channel dimension. The first dimension is pitched for more efficient memory access and compatibility with CUDA texture and surface objects.

`DataProcessor` is an abstract base class that accepts a pointer to a `DataArray` as input, applies some user-defined processing functions, and produces a `DataArray` as output. `DataProcessor`s can be tagged as belonging to specific CUDA streams to enable concurrent execution and/or multi-GPU processing. Currently, `gpuBF` is composed of the following `DataProcessor`s:

* `Focus`
  Applies specified time delays to channel data. Assumes one transmit beam to one receive beam.
* `FocusSynap`
  Applies specified time delays to channel data with retrospective aperture synthesis (e.g., plane wave imaging). Can be used to coherently sum multiple transmit firings or receive elements in a focused manner.
* `ChannelSum`
  Sums the channels into `N` equal subapertures. By default, `N = 1;`, i.e., all channels are summed into a single signal.
* `Bmode`
  Applies envelope detection, and optionally, logarithmic compression. If more than one channel is provided (`nc > 1`), the respective images are summed incoherently (e.g., spatial compounding).
* `NeuralNetwork`
  Uses NVIDIA's TensorRT framework to perform real-time inference of an artificial neural network. `NeuralNetwork` converts a frozen TensorFlow graph with a single input and a single output node. See `annBF` below for more details.
* In development: `HilbertTransform`, `GenericFilter`, `EnsembleFilter`, `PowerEstimator`, `VelocityEstimator`.

### vsxBF

`vsxBF` provides an interface between `gpuBF` and a Verasonics research ultrasound system. Several utilities are included, such as the `VSXDataFormatter`, which applies Verasonics-specific formatting to the raw input data stream (e.g., direct quadrature sampling), as well as utilities to copy GPU data to MATLAB arrays. Several example scripts are provided as well.

### annBF

`annBF` consists of python implementations of artificial neural networks that are saved into a format that can be loaded into `gpuBF` via NVIDIA's TensorRT API. TensorRT is currently restricted to a small list of supported operations, but as this list expands, it will become possible to implement custom beamforming algorithms as neural networks (whether they include machine learning or not), with optimization performed via TensorRT rather than manual CUDA kernel tuning.
