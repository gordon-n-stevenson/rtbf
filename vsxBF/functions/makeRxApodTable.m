%% MATLAB focusing kernel
function apoRx = makeRxApodTable(elemPos, pixPos, fnum, apers_0)

if nargin == 4 && length(unique(apers_0)) == 1
	elemPos = elemPos( int32(1:128) + apers_0(1), :);
end

nelems = size(elemPos,1);
nrows = size(pixPos,1);
ncols = size(pixPos,2);
apoRx = zeros(nrows,nelems,ncols,'single');
pitch = sqrt(sum((elemPos(2,:)-elemPos(1,:)).^2));

zpos = pixPos(:,1,3);
xpos = pixPos(1,:,1);

for E = 1:nelems
	epos = elemPos(E,:);
	idx = abs( bsxfun(@rdivide, zpos, (epos(1)-xpos)) ) > fnum;
	% Set apodization of valid indices to 1, invalid indices to 1e-4.
	% This avoids having a NaN issue later on with SLSC.
	apo = zeros(nrows, ncols, 'single');
	apo(idx) = 1;
	apoRx(:,E,:) = apo;
end

% Make sure that there are always at least 10 elements active.
% Use the 10 elements closest to the pixel x-location
% ONLY FOR LINEAR ARRAY TRANSDUCERS...
if all(elemPos(:,3) == 0)
	for C = 1:ncols
		apo = false(nrows, nelems);
		x = xpos(C);
		[~,tmp] = sort(abs(x - elemPos(:,1)));
		apo(:, tmp(1:10)) = true;
		apoRx(:,:,C) = (apoRx(:,:,C) > 0) | apo;
	end
end

% apoRx = bsxfun(@rdivide, apoRx, sum(apoRx,2)/nelems);
apoRx(apoRx == 0) = 1e-4;
