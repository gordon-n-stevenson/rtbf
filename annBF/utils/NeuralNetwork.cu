/**
 @file annBF/utils/NeuralNetwork.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "NeuralNetwork.cuh"

namespace rtbf {
template <typename T_in, typename T_out>
NeuralNetwork<T_in, T_out>::NeuralNetwork() {
  this->resetDataProcessor();
  reset();
}
template <typename T_in, typename T_out>
NeuralNetwork<T_in, T_out>::~NeuralNetwork() {
  reset();
}
template <typename T_in, typename T_out>
void NeuralNetwork<T_in, T_out>::reset() {
  if (this->isInit) this->printMsg("Clearing object.");
  CCE(cudaSetDevice(this->devID));

  // NeuralNetwork class members
  in = nullptr;
  context = nullptr;
  engine = nullptr;
  buffers[0] = nullptr;
  buffers[1] = nullptr;
  iBufIdx = 0;
  oBufIdx = 0;

  // Base class members
  this->resetDataProcessor();
  strcpy(this->moniker, "NeuralNetwork");
}
template <typename T_in, typename T_out>
void NeuralNetwork<T_in, T_out>::initialize(DataArray<T_in> *input,
                                            const char *networkFileName,
                                            const char *inputNodeName,
                                            const char *outputNodeName,
                                            cudaStream_t cudaStream,
                                            int verbosity) {
  // If previously initialized, reset
  reset();

  // Device information
  this->setDeviceID(input->getDeviceID());
  this->stream = cudaStream;
  this->verb = verbosity;
  this->printMsg("Initializing object.");

  // Parse inputs
  in = input;
  strcpy(net_filename, networkFileName);
  strcpy(input_node, inputNodeName);
  strcpy(output_node, outputNodeName);

  // Determine if .onnx file or .uff file
  // Naive approach, but does not depend on C++11 std library
  int fnamelen = strlen(net_filename);
  if (strcmp(&net_filename[fnamelen - 4], ".uff") == 0) {
    net_filetype = NeuralNetwork::FileType::UFF;
  } else if (strcmp(&net_filename[fnamelen - 5], ".onnx") == 0) {
    net_filetype = NeuralNetwork::FileType::ONNX;
  } else {
    fprintf(
        stderr,
        "Invalid network file %s. Must have extension \".uff\" or \".onnx\".\n",
        net_filename);
  }

  // Create output DataArray
  // Set the first dimension to be equal to the pitch of the input array.
  // This is to avoid any mismatch in pitch due to datatypes.
  this->out.initialize(in->getNp(), in->getNy(), 1, in->getNf(), this->devID,
                       "NeuralNetwork", this->verb, false);

  // Initialize neural network engine
  CCE(cudaSetDevice(this->devID));
  T_in dummy;
  dummy *= 0;  // Suppress warning about dummy being used before set
  int vecsize = checkVectorLength(dummy);

  // Build engine according to network file type
  if (net_filetype == NeuralNetwork::FileType::UFF) {
    // Build network definition from frozen graph
    nvinfer1::IBuilder *builder = nvinfer1::createInferBuilder(gLogger);
    builder->setMaxBatchSize(1);
    builder->setMaxWorkspaceSize(1 << 20);
    nvinfer1::INetworkDefinition *network = builder->createNetwork();
    // Build the engine
    auto parser = nvuffparser::createUffParser();
    parser->registerInput(
        input_node,
        nvinfer1::DimsCHW(in->getNc(), in->getNy(), in->getNp() * vecsize),
        nvuffparser::UffInputOrder::kNCHW);
    parser->registerOutput(output_node);
    if (!parser->parse(net_filename, *network, nvinfer1::DataType::kFLOAT))
      this->printErr("Failed to parse the UFF file.", __FILE__, __LINE__);
    engine = builder->buildCudaEngine(*network);
    // We can destroy the network and the parser
    network->destroy();
    builder->destroy();
    // Create an execution context
    context = engine->createExecutionContext();
    // In order to bind the buffers, we need to know the names of the
    // input and output tensors
    iBufIdx = engine->getBindingIndex(input_node);
    oBufIdx = engine->getBindingIndex(output_node);
  } else if (net_filetype == NeuralNetwork::FileType::ONNX) {
    // Build network definition from frozen graph
    nvinfer1::IBuilder *builder = nvinfer1::createInferBuilder(gLogger);
    const auto expBatch =
        1U << static_cast<uint32_t>(
            nvinfer1::NetworkDefinitionCreationFlag::kEXPLICIT_BATCH);
    builder->setMaxBatchSize(1);
    builder->setMaxWorkspaceSize(1 << 20);
    nvinfer1::INetworkDefinition *network = builder->createNetworkV2(expBatch);
    // Build the engine
    nvonnxparser::IParser *parser =
        nvonnxparser::createParser(*network, gLogger);
    if (!parser->parseFromFile(net_filename,
                               int(nvinfer1::ILogger::Severity::kWARNING)))
      this->printErr("Failed to parse the ONNX file.", __FILE__, __LINE__);
    engine = builder->buildCudaEngine(*network);
    // We can destroy the network and the parser
    network->destroy();
    builder->destroy();
    // Create an execution context
    context = engine->createExecutionContext();
    // In order to bind the buffers, we need to know the names of the
    // input and output tensors
    iBufIdx = engine->getBindingIndex(input_node);
    oBufIdx = engine->getBindingIndex(output_node);
  }

  // Mark as initialized
  this->isInit = true;
}
template <typename T_in, typename T_out>
void NeuralNetwork<T_in, T_out>::initialize(DataProcessor<T_in> *input,
                                            const char *networkFileName,
                                            const char *inputNodeName,
                                            const char *outputNodeName,
                                            cudaStream_t cudaStream,
                                            int verbosity) {
  // Pass through to initializer
  initialize(input->getOutputDataArray(), networkFileName, inputNodeName,
             outputNodeName, cudaStream, verbosity);
}

template <typename T_in, typename T_out>
void NeuralNetwork<T_in, T_out>::setInputDataArray(DataArray<T_in> *input) {
  if (!this->isInit) {
    this->printErr("Object not initialized.", __FILE__, __LINE__);
    return;
  }
  if (!(in->getDataDim() == input->getDataDim())) {
    this->printErr("New input DataArray dimensions do not match.", __FILE__,
                   __LINE__);
    return;
  }
  in = input;
}

template <typename T_in, typename T_out>
void NeuralNetwork<T_in, T_out>::execute() {
  // Execute the neural network!
  for (int frame = 0; frame < in->getNf(); frame++) {
    buffers[iBufIdx] = in->getFramePtr(frame);
    buffers[oBufIdx] = this->out.getFramePtr(frame);
    bool success;
    success = context->enqueue(1, buffers, this->stream, nullptr);
    if (!success) printf("Context execution failed.\n");
  }
}

template class NeuralNetwork<float2, float>;

}  // namespace rtbf
